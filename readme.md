# Configurer son vim

## Installation de plug et création du .vimrc

```shell
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
cp vimrc ~/.vimrc
```

## Ajouter la correction orthographique

```shell
setlocal spell
set spelllang=fr
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u
```

# Configurer ses variables d'environnement

```shell
cp env ~/.env
```

Ajouter à la fin de `~/.profil` :

```shell
if [ -e "$HOME/.env" ] ; then
    source "$HOME/.env"
fi
```